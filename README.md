# lets_go

Nama : **Muhamad Hidayatulloh**

Npm : **18282008**

> lets_go adalah penyedia jasa angkutan online, kemudian aplikasi ini juga dibuat untuk memenuhi tugas matakuliah Mobile Programming 2


## Sreenshoots
============================
<img src="https://gitlab.com/muhhi73/lets_go/-/raw/master/screenshoots/screenshot_page_profile.jpg" width="300" height="800">
=============================
<img src="https://gitlab.com/muhhi73/lets_go/-/raw/master/screenshoots/about_page.jpg" width="300" height="800">
============================
<img src="https://gitlab.com/muhhi73/lets_go/-/raw/master/screenshoots/form_input.jpg" width="300" height="800">
===========================
<img src="https://gitlab.com/muhhi73/lets_go/-/raw/master/screenshoots/home_page.jpg" width="300" height="800">
============================
<img src="https://gitlab.com/muhhi73/lets_go/-/raw/master/screenshoots/login_page.jpg" width="300" height="800">
=============================
<img src="https://gitlab.com/muhhi73/lets_go/-/raw/master/screenshoots/register_page.jpg" width="300" height="800">
============================
<img src="https://gitlab.com/muhhi73/lets_go/-/raw/master/screenshoots/riwayat_page.jpg" width="300" height="800">
============================
<img src="https://gitlab.com/muhhi73/lets_go/-/raw/master/screenshoots/welcome_page.jpg" width="300" height="800">


## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.
