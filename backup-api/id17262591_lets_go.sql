-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Waktu pembuatan: 27 Jul 2021 pada 23.47
-- Versi server: 10.3.16-MariaDB
-- Versi PHP: 7.3.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `id17262591_lets_go`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `kendaraan`
--

CREATE TABLE `kendaraan` (
  `id` int(11) NOT NULL,
  `jenis_kendaraan` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `harga` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `kendaraan`
--

INSERT INTO `kendaraan` (`id`, `jenis_kendaraan`, `harga`) VALUES
(1, 'taksi', 20000),
(2, 'Mini Bus (4 orang)', 10000);

-- --------------------------------------------------------

--
-- Struktur dari tabel `pemesanan`
--

CREATE TABLE `pemesanan` (
  `id` int(11) NOT NULL,
  `id_kendaraan` int(11) NOT NULL,
  `posisi_awal` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tujuan` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(2) NOT NULL DEFAULT 0,
  `created` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `pemesanan`
--

INSERT INTO `pemesanan` (`id`, `id_kendaraan`, `posisi_awal`, `tujuan`, `status`, `created`) VALUES
(1, 1, 'jakarta', 'sumedang', 1, '2021-07-27 00:23:48'),
(2, 2, 'bandung', 'cianjur', 0, '2021-07-27 00:25:39'),
(3, 1, 'Purwakarta', 'Indramayu', 0, '2021-07-27 09:51:07'),
(4, 2, 'Purwakarta', 'Bandung', 0, '2021-07-27 10:01:34'),
(5, 1, 'Ciamis', 'Majalengka', 0, '2021-07-27 10:06:52');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL DEFAULT current_timestamp(),
  `updated` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id`, `nama`, `username`, `password`, `created`, `updated`) VALUES
(1, 'Muhamad Hidayatulloh', 'muhhi', 'muhhi', '2021-07-17 05:44:34', '2021-07-17 05:44:34'),
(2, 'Rafli Arya Wiguna', 'rafli', 'rafli', '2021-07-17 05:44:34', '2021-07-17 05:45:09'),
(3, 'Arif Reyhan', 'arif', 'arif', '2021-07-17 07:42:57', '2021-07-17 07:42:57'),
(5, 'Dwi Candra Gumelar', 'candra', 'candra', '2021-07-17 08:21:04', '2021-07-17 08:21:04'),
(6, 'Harnum Anita Sholiha', 'anung', 'anung', '2021-07-17 15:40:59', '2021-07-17 15:40:59'),
(7, 'Risca', 'ica', 'ica', '2021-07-18 04:10:23', '2021-07-18 04:10:23'),
(8, 'Ponco', 'ponco', 'ponco', '2021-07-23 04:09:59', '2021-07-23 04:09:59'),
(9, 'Humayah', 'humayah', 'humayah', '2021-07-24 07:21:20', '2021-07-24 07:21:20'),
(10, 'Kodir', 'kodir', 'kodir', '2021-07-24 07:21:59', '2021-07-24 07:21:59');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `kendaraan`
--
ALTER TABLE `kendaraan`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `pemesanan`
--
ALTER TABLE `pemesanan`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `kendaraan`
--
ALTER TABLE `kendaraan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `pemesanan`
--
ALTER TABLE `pemesanan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
