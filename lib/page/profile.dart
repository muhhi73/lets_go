import 'package:flutter/material.dart';
import 'package:lets_go/beranda/beranda_appbar.dart';
import 'package:lets_go/model/get_model.dart';
import 'package:lets_go/model/model.dart';
import 'package:lets_go/page/user_screen.dart';

class Profile extends StatefulWidget {
  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  UserGet userGet;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    UserGet.connectToApiUser('8').then((value) {
      setState(() {
        userGet = value;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(appBar: LetsAppBar(), body: _bodyProfile()),
    );
  }

  Widget _bodyProfile() {
    return Center(
      child: Container(
        height: MediaQuery.of(context).size.height,
        color: Colors.blue[100],
        alignment: Alignment.center,
        padding: EdgeInsets.all(30),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Text(
              "WELCOME!",
              style: TextStyle(fontSize: 48, color: Color(0xff0563BA)),
            ),
            Container(
                child: Column(
              children: [
                ClipRRect(
                  borderRadius: BorderRadius.circular(70.0),
                  child: Image.network(
                    (userGet != null)
                        ? userGet.avatar
                        : "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS3YaKRGHiI0Jgg7Xp35rFtHQHS8GTPqGINEg&usqp=CAU",
                    cacheHeight: 128,
                    cacheWidth: 128,
                    fit: BoxFit.cover,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    (userGet != null)
                        ? userGet.firstName + " " + userGet.lastName
                        : "Data Gagal Diambil",
                    style: TextStyle(fontSize: 24, color: Color(0xff0563BA)),
                  ),
                ),
                Text(
                  (userGet != null) ? userGet.email : "Data gagal Diambil",
                  style: TextStyle(fontSize: 18, color: Colors.black),
                ),
                RaisedButton.icon(
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => UserScreen()));
                    },
                    icon: Icon(Icons.person),
                    label: Text('User View'))
              ],
            ))
          ],
        ),
      ),
    );
  }
}
