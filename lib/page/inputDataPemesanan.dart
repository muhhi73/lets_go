import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:http/http.dart' as http;
import 'package:lets_go/model/model.dart';
import 'package:lets_go/model/postmodel.dart';

class InputPemesananPage extends StatefulWidget {
  // String titleAppbar;
  // InputPemesananPage({this.titleAppbar});
  @override
  State<StatefulWidget> createState() {
    return InputPemesananPageState();
  }
}

class InputPemesananPageState extends State<InputPemesananPage> {
  TextEditingController _posisiAwal = TextEditingController();
  TextEditingController _tujuan = TextEditingController();
  TextEditingController _jenisKendaraan = TextEditingController();
  // TextEditingController rollnoctl = TextEditingController();
  //text controller for TextField

  bool error, sending, success;
  String msg;

  String phpurl = "http://lets-go.epizy.com/addData.php";
  // do not use http://localhost/ for your local
  // machine, Android emulation do not recognize localhost
  // insted use your local ip address or your live URL
  // hit "ipconfig" on Windows or  "ip a" on Linux to get IP Address

  @override
  void initState() {
    error = false;
    sending = false;
    success = false;
    msg = "";
    super.initState();
  }

  // void sendData() async {

  //   if (res.statusCode == 200) {
  //     print(res.body); //print raw response on console
  //     var data = json.decode(res.body); //decoding json to array
  //     if (data["error"]) {
  //       setState(() {
  //         //refresh the UI when error is recieved from server
  //         sending = false;
  //         error = true;
  //         msg = data["message"]; //error message from server
  //       });
  //     } else {
  //       nama.text = "";
  //       noTelp.text = "";
  //       gambarProfile.text = "";

  //       //after write success, make fields empty

  //       setState(() {
  //         sending = false;
  //         success = true; //mark success and refresh UI with setState
  //       });
  //     }
  //   } else {
  //     //there is error
  //     setState(() {
  //       error = true;
  //       msg = "Error during sendign data.";
  //       sending = false;
  //       //mark error and refresh UI with setState
  //     });
  //   }
  // }

  void addData() {
    PemesananPostModel commRequest = PemesananPostModel();
    commRequest.posisiAwal = _posisiAwal.text;
    commRequest.tujuan = _tujuan.text;
    commRequest.jenisKendaraan = _jenisKendaraan.text;

    PemesananModel().post(pemesananpostModelToJson(commRequest)).then((value) {
      if (value == true) {
        setState(() {
          sending = false;
          success = true; //mark success and refresh UI with setState
        });
      } else {
        setState(() {
          msg = "Data Tidak Terkirim";
          sending = false;
          success = true; //mark success and refresh UI with setState
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Text("Pemesanan"), backgroundColor: Colors.redAccent), //appbar

      body: SingleChildScrollView(
          //enable scrolling, when keyboard appears,
          // hight becomes small, so prevent overflow
          child: Container(
              padding: EdgeInsets.all(20),
              child: Column(
                children: <Widget>[
                  Container(
                    child: Text(error ? msg : "Masukkan Pesanan Anda"),
                    //if there is error then sho msg, other wise show text message
                  ),

                  Container(
                      child: TextField(
                    controller: _posisiAwal,
                    decoration: InputDecoration(
                      labelText: "Posisi Awal",
                      hintText: "Masukkan Posisi Awal Anda @ex Bandung",
                    ),
                  )), //text input for nama

                  Container(
                      child: TextField(
                    controller: _tujuan,
                    decoration: InputDecoration(
                      labelText: "Tujuan",
                      hintText: "Masukkan tujuan anda @ex Indramayu",
                    ),
                  )),

                  Container(
                    margin: EdgeInsets.only(top: 8.0),
                    child: Text("Pilih Kendaraan"),
                    //if there is error then sho msg, other wise show text message
                  ),
                  RaisedButton(
                    onPressed: () {
                      _jenisKendaraan.value = TextEditingValue(text: "1");
                    },
                    child: Text("Taksi"),
                  ),
                  RaisedButton(
                    onPressed: () {
                      _jenisKendaraan.value = TextEditingValue(text: "2");
                    },
                    child: Text("Mini Bus (4 Orang)"),
                  ),
                  Container(
                      child: TextField(
                    readOnly: true,
                    controller: _jenisKendaraan,
                    decoration: InputDecoration(
                      labelText: "(tidak Perlu di isi, biarkan seperti ini)",
                    ),
                  )),

                  //text input for gambar

                  Container(
                      margin: EdgeInsets.only(top: 20),
                      child: SizedBox(
                          width: double.infinity,
                          child: RaisedButton(
                            onPressed: () {
                              //if button is pressed, setstate sending = true, so that we can show "sending..."
                              setState(() {
                                sending = true;
                              });
                              addData();
                              _jenisKendaraan.value =
                                  TextEditingValue(text: "");
                              _posisiAwal.value = TextEditingValue(text: "");
                              _tujuan.value = TextEditingValue(text: "");
                            },
                            child: Text(
                              sending ? "Sending..." : "SEND DATA",
                              //if sending == true then show "Sending" else show "SEND DATA";
                            ),
                            color: Colors.redAccent,
                            colorBrightness: Brightness.dark,
                            //background of button is darker color, so set brightness to dark
                          ))),

                  Container(
                    color: Colors.green,
                    padding: EdgeInsets.all(8.0),
                    child: Text(
                      success ? "Data Sukses Terkirim" : "Mengirim Data",
                      style: TextStyle(fontSize: 20.0),
                    ),
                    //is there is success then show "Write Success" else show "send data"
                  ),
                ],
              ))),
    );
  }
}
