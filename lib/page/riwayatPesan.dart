import 'package:flutter/material.dart';
import 'package:lets_go/beranda/beranda_appbar.dart';
import 'package:lets_go/model/model.dart';

class RiwayatPemesananView extends StatefulWidget {
  @override
  _RiwayatPemesananViewState createState() => _RiwayatPemesananViewState();
}

class _RiwayatPemesananViewState extends State<RiwayatPemesananView> {
  List dataPemesanan = new List();

  void getDataPemesanan() {
    PemesananModel().getPemesanan().then((value) {
      setState(() {
        dataPemesanan = value;
      });
    });
  }

  @override
  void initState() {
    super.initState();
    getDataPemesanan();
  }

  Widget _header(int i) {
    return new Container(
      padding: EdgeInsets.all(12.0),
      decoration: new BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            colors: [const Color(0xff3164bd), const Color(0xff295cb5)],
          ),
          borderRadius: new BorderRadius.only(
              topLeft: new Radius.circular(8.0),
              topRight: new Radius.circular(8.0))),
      child: new Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          // tanggal pemesanan
          new Text(
            dataPemesanan[i]['created'],
            style: new TextStyle(
                fontSize: 14.0, color: Colors.white, fontFamily: "Avilar"),
          ),
          // status proses pemesanan
          new Container(
            child: new Text(
              dataPemesanan[i]['status'] == '0'
                  ? "Sedang Proses"
                  : dataPemesanan[i]['status'] == '1'
                      ? "Selesai"
                      : "Batal",
              style: new TextStyle(
                  fontSize: 14.0, color: Colors.white, fontFamily: "Avilar"),
            ),
          )
        ],
      ),
    );
  }

  Widget _body(int i) {
    return new Container(
      padding:
          EdgeInsets.only(left: 32.0, right: 32.0, top: 12.0, bottom: 12.0),
      child: new Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          // posisi awal
          new Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              new Icon(
                Icons.my_location_rounded,
                size: 24,
                color: Colors.orange[900],
              ),
              new Padding(
                padding: EdgeInsets.only(top: 10.0),
              ),
              new Text(
                dataPemesanan[i]['posisiAwal'],
                style: TextStyle(color: Colors.black, fontSize: 15.0),
              ),
            ],
          ),

          // tujuan
          new Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              new Icon(
                Icons.location_on,
                size: 24,
                color: Colors.orange[900],
              ),
              new Padding(
                padding: EdgeInsets.only(top: 10.0),
              ),
              new Text(
                dataPemesanan[i]['tujuan'],
                style: TextStyle(color: Colors.black, fontSize: 15.0),
              )
            ],
          ),

          // tarif
          new Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Center(
                child: new Text(
                  "Rp. " + dataPemesanan[i]['harga'] + "/Km",
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 15.0,
                      fontWeight: FontWeight.bold),
                ),
              )
            ],
          ),
        ],
      ),
    );
  }

  Widget _footer(int i) {
    return new Container(
      padding: EdgeInsets.all(12.0),
      decoration: new BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            colors: [const Color(0xff3164bd), const Color(0xff295cb5)],
          ),
          borderRadius: new BorderRadius.only(
              bottomLeft: new Radius.circular(10.0),
              bottomRight: new Radius.circular(10.0))),
      child: new Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              new Text(
                "Jenis Kendaraan  :",
                style: new TextStyle(
                    fontSize: 14.0, color: Colors.white, fontFamily: "Avilar"),
              ),
              new Text(
                "Metode Pembayaran  :",
                style: new TextStyle(
                    fontSize: 14.0, color: Colors.white, fontFamily: "Avilar"),
              ),
            ],
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              new Text(
                dataPemesanan[i]['jenisKendaraan'],
                style: new TextStyle(
                    fontSize: 14.0, color: Colors.white, fontFamily: "Avilar"),
              ),
              new Text(
                "Tunai",
                style: new TextStyle(
                    fontSize: 14.0, color: Colors.white, fontFamily: "Avilar"),
              ),
            ],
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
            appBar: LetsAppBar(),
            body: Container(
                padding: EdgeInsets.all(10.0),
                child: dataPemesanan == null
                    ? Center(
                        child: CircularProgressIndicator(),
                      )
                    : ListView.builder(
                        itemCount: dataPemesanan.length,
                        itemBuilder: (context, i) {
                          return new Container(
                            margin: EdgeInsets.only(top: 10.0),
                            decoration: new BoxDecoration(
                                gradient: LinearGradient(
                                  begin: Alignment.topCenter,
                                  end: Alignment.bottomCenter,
                                  colors: [
                                    Colors.orange[100],
                                    const Color(0xffe99e1e)
                                  ],
                                ),
                                borderRadius: new BorderRadius.all(
                                    new Radius.circular(8.0))),
                            child: new Column(
                              children: <Widget>[
                                _header(i),
                                _body(i),
                                _footer(i),
                              ],
                            ),
                          );
                        },
                      ))));
  }
}
