import 'package:flutter/material.dart';
import 'package:lets_go/login/login_view.dart';
import 'package:lets_go/login/signup.dart';
import 'package:google_fonts/google_fonts.dart';

class AboutPage extends StatefulWidget {
  AboutPage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _AboutPageState createState() => _AboutPageState();
}

class _AboutPageState extends State<AboutPage> {
  Widget _label() {
    return Container(
        margin: EdgeInsets.only(top: 40),
        padding: EdgeInsets.all(15),
        color: Colors.black26,
        child: Column(
          children: <Widget>[
            Text(
              "Copyright Muhamad Hidayatulloh, 18282008",
              style: TextStyle(color: Colors.white, fontSize: 17),
            ),
            SizedBox(
              height: 20,
            ),
            Center(
              child: Text(
                "@Muhhi's Comp",
                style: TextStyle(color: Colors.white, fontSize: 17),
              ),
            )
          ],
        ));
  }

  Widget _title() {
    return RichText(
      textAlign: TextAlign.center,
      text: TextSpan(
          text: '',
          style: GoogleFonts.portLligatSans(
            textStyle: Theme.of(context).textTheme.display1,
            fontSize: 30,
            fontWeight: FontWeight.w700,
            color: Colors.white,
          ),
          children: [
            TextSpan(
              text: "Let's GO ",
              style: TextStyle(color: Colors.black, fontSize: 23),
            ),
            TextSpan(
              text:
                  'adalah sebuah aplikasi penyedia jasa secara online, kemudian aplikasi ini juga dibuat untuk memenuhi tugas mata kuliah Mobile Programming',
              style: TextStyle(color: Colors.white, fontSize: 23),
            ),
          ]),
    );
  }

  Widget _brand() {
    String src =
        "https://payload.cargocollective.com/1/6/216449/4883114/letsgo2.png";
    return new Container(
      child: new Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Center(
              child: new Image.network(
            src,
            height: 150,
            width: 250,
          ))
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 20),
          height: MediaQuery.of(context).size.height,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(5)),
              boxShadow: <BoxShadow>[
                BoxShadow(
                    color: Colors.grey.shade200,
                    offset: Offset(2, 4),
                    blurRadius: 5,
                    spreadRadius: 2)
              ],
              gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [Color(0xfffbb448), Color(0xffe46b10)])),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              _brand(),
              // _title(),
              SizedBox(
                height: 60,
              ),

              _title(),

              SizedBox(
                height: 40,
              ),
              _label()
            ],
          ),
        ),
      ),
    );
  }
}
