import 'package:flutter/material.dart';
import 'package:lets_go/beranda/beranda_appbar.dart';
import 'package:lets_go/model/model.dart';
import 'package:lets_go/model/user_model.dart';

class UserScreen extends StatefulWidget {
  @override
  _UserScreenState createState() => _UserScreenState();
}

class _UserScreenState extends State<UserScreen> {
  List dataUser = new List();

  void getDataUser() {
    UserViewModel().getUsers().then((value) {
      setState(() {
        dataUser = value;
      });
    });
  }

  @override
  void initState() {
    super.initState();
    getDataUser();
  }

  Widget _judul() {
    return Container(
      child: Text("My Profile"),
    );
  }

  Widget _body() {
    return Container(
        child: dataUser == null
            ? Center(
                child: CircularProgressIndicator(),
              )
            : ListView.builder(
                itemCount: dataUser.length,
                itemBuilder: (context, i) {
                  return Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Text(dataUser[i]['nama'] ??
                          "Gagal Mengambil data nama user"),
                      Text(dataUser[i]['username'] ??
                          "Gagal Mengambil data username user"),
                    ],
                    //               child: ListTile(
                    // title: Text(dataUser[i]['nama'] ?? 'gak ada')),
                  );
                },
              ));
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(appBar: LetsAppBar(), body: _body()),
    );
  }
}
