import 'package:http/http.dart' as http;
import 'dart:convert';

class User {
  String id, nama, noTlp, gambarProfile;

  User({this.id, this.nama, this.noTlp});

  factory User.createUser(Map<String, dynamic> object) {
    return User(nama: object['nama'], noTlp: object['no_tlp']);
  }

  static Future<User> apiUserGet() async {
    String url = "http://lets-go.epizy.com/getData.php";

    var result = await http.get(url);

    var jsonObject = json.decode(result.body);

    var userData = (jsonObject as Map<String, dynamic>)['result'];

    return User.createUser(userData);
  }
}
