import 'dart:convert';

// UserpostModel userpostModelFromJson(String str) =>
//     UserpostModel.fromJson(json.decode(str));
List userModelFromJson(String str) => List.from(json.decode(str));
//.map((x) => UserpostModel.fromJson(x))
String userpostModelToJson(UserpostModel data) => json.encode(data.toJson());
String pemesananpostModelToJson(PemesananPostModel data) =>
    json.encode(data.toJsonPemesanan());

class UserpostModel {
  UserpostModel({
    this.namaLengkap,
    this.username,
    this.password,
  });

  String namaLengkap;
  String username;
  String password;

  Map toJson() => {
        "namaLengkap": namaLengkap,
        "username": username,
        "password": password,
      };
}

class PemesananPostModel {
  String posisiAwal, tujuan, jenisKendaraan;
  PemesananPostModel({this.posisiAwal, this.tujuan, this.jenisKendaraan});

  Map toJsonPemesanan() => {
        'posisiAwal': posisiAwal,
        'tujuan': tujuan,
        'jenisKendaraan': jenisKendaraan
      };
}
