import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:lets_go/model/postmodel.dart';

class UserViewModel {
  Future getUsers() async {
    try {
      http.Response hasil = await http.get(
          Uri.encodeFull("http://lets-goapp.000webhostapp.com/read.php"),
          headers: {"Accept": "application/json"});
      if (hasil.statusCode == 200) {
        print("data category success");
        final data = userModelFromJson(hasil.body);
        print(data);
        return data;
      } else {
        print("error status " + hasil.statusCode.toString());
        return null;
      }
    } catch (e) {
      print("error catch $e");
      return null;
    }
  }
  // Future<List<UserpostModel>> getUsers() async {
  //   try {
  //     http.Response hasil = await http.get(
  //         Uri.encodeFull("http://lets-goapp.000webhostapp.com/read.php"),
  //         headers: {"Accept": "application/json"});
  //     if (hasil.statusCode == 200) {
  //       final items =
  //           userpostModelFromJson(hasil.body).cast<Map<String, dynamic>>();
  //     } else {
  //       print("error status " + hasil.statusCode.toString());
  //       return null;
  //     }
  //   } catch (e) {
  //     print("error catch $e");
  //     return null;
  //   }
  // }

  Future postUser(body) async {
    try {
      http.Response hasil = await http.post(
          Uri.encodeFull("http://lets-goapp.000webhostapp.com/create.php"),
          body: body,
          headers: {
            "Accept": "application/json",
          });
      if (hasil.statusCode == 200) {
        print("status 200");
        // final data = json.decode(hasil.body);
        return true;
      } else {
        print("error status " + hasil.statusCode.toString());
        return null;
      }
    } catch (e) {
      print("error catchnya $e");
      return null;
    }
  }
}

class PemesananModel {
  Future getPemesanan() async {
    try {
      http.Response hasil = await http.get(
          Uri.encodeFull("https://lets-goapp.000webhostapp.com/pemesanan.php"),
          headers: {"Accept": "application/json"});
      if (hasil.statusCode == 200) {
        print("Data Pemesanan Berhasil Diambil");
        final data = userModelFromJson(hasil.body);
        print(data);
        return data;
      } else {
        print("error status " + hasil.statusCode.toString());
        return null;
      }
    } catch (e) {
      print("error catch $e");
      return null;
    }
  }

  Future post(body) async {
    try {
      http.Response hasil = await http.post(
          Uri.encodeFull(
              "http://lets-goapp.000webhostapp.com/addPemesanan.php"),
          body: body,
          headers: {
            "Accept": "application/json",
          });
      if (hasil.statusCode == 200) {
        print("status 200");
        // final data = json.decode(hasil.body);
        return true;
      } else {
        print("error status " + hasil.statusCode.toString());
        return null;
      }
    } catch (e) {
      print("error catchnya $e");
      return null;
    }
  }
}
