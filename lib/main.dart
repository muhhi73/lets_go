import 'package:flutter/material.dart';
import 'package:lets_go/launcher/launcher_view.dart';
import 'package:lets_go/constant.dart';
import 'package:flutter/services.dart';
import 'package:lets_go/login/welcome_page_view.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Let'sGo",
      theme: ThemeData(
        fontFamily: 'Avilar',
        primaryColor: LetsPalette.menuRide,
        accentColor: LetsPalette.menuRide,
      ),
      // home: LauncherPage(),
      home: WelcomePage(),
    );
  }
}
