import 'package:flutter/material.dart';
import 'package:lets_go/beranda/beranda_appbar.dart';
import 'package:lets_go/constant.dart';
import 'package:lets_go/beranda/beranda_model.dart';
import 'package:lets_go/page/inputDataPemesanan.dart';

class BerandaPage extends StatefulWidget {
  @override
  _BerandaPageState createState() => _BerandaPageState();
}

class _BerandaPageState extends State<BerandaPage> {
  @override
  Widget build(BuildContext context) {
    return new SafeArea(
      child: Scaffold(
        appBar: new LetsAppBar(),
        backgroundColor: Color(0x50FFD180),
        body: new Container(
          child: new ListView(
            physics: ClampingScrollPhysics(),
            children: <Widget>[
              new Container(
                  padding: EdgeInsets.only(left: 16.0, right: 16.0, top: 16.0),
                  color: Colors.white,
                  child: new Column(
                    children: <Widget>[
                      _buildLetsPayMenu(),
                      _buildLetsgoServiceMenu(),
                    ],
                  )),
              new Container(
                color: Colors.white,
                margin: EdgeInsets.only(top: 16.0),
                child: new Column(
                  children: <Widget>[
                    _buildLetsFoodFeatured(),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

// untuk menampilkan menu letspay
  Widget _buildLetsPayMenu() {
    return new Container(
        height: 120.0,
        decoration: new BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [const Color(0xff3164bd), const Color(0xff295cb5)],
            ),
            borderRadius: new BorderRadius.all(new Radius.circular(3.0))),
        child: new Column(
          children: <Widget>[
            new Container(
              padding: EdgeInsets.all(12.0),
              decoration: new BoxDecoration(
                  gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    colors: [const Color(0xff3164bd), const Color(0xff295cb5)],
                  ),
                  borderRadius: new BorderRadius.only(
                      topLeft: new Radius.circular(3.0),
                      topRight: new Radius.circular(3.0))),
              child: new Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  new Text(
                    "Let'sPay",
                    style: new TextStyle(
                        fontSize: 18.0,
                        color: Colors.white,
                        fontFamily: "Avilar"),
                  ),
                  new Container(
                    child: new Text(
                      "Rp. 120.000",
                      style: new TextStyle(
                          fontSize: 14.0,
                          color: Colors.white,
                          fontFamily: "Avilar"),
                    ),
                  )
                ],
              ),
            ),
            new Container(
              padding: EdgeInsets.only(left: 32.0, right: 32.0, top: 12.0),
              child: new Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  new Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      new Icon(
                        Icons.transform_outlined,
                        size: 32,
                        color: Colors.white,
                      ),
                      new Padding(
                        padding: EdgeInsets.only(top: 10.0),
                      ),
                      new Text(
                        "Transfer",
                        style: TextStyle(color: Colors.white, fontSize: 12.0),
                      )
                    ],
                  ),
                  new Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      new Icon(
                        Icons.qr_code_scanner_outlined,
                        size: 32,
                        color: Colors.white,
                      ),
                      new Padding(
                        padding: EdgeInsets.only(top: 10.0),
                      ),
                      new Text(
                        "Scan QR",
                        style: TextStyle(color: Colors.white, fontSize: 12.0),
                      )
                    ],
                  ),
                  new Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      new Icon(
                        Icons.add_outlined,
                        size: 32,
                        color: Colors.white,
                      ),
                      new Padding(
                        padding: EdgeInsets.only(top: 10.0),
                      ),
                      new Text(
                        "Isi Saldo",
                        style: TextStyle(color: Colors.white, fontSize: 12.0),
                      )
                    ],
                  ),
                  new Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      new Icon(
                        Icons.list_alt_outlined,
                        size: 32,
                        color: Colors.white,
                      ),
                      new Padding(
                        padding: EdgeInsets.only(top: 10.0),
                      ),
                      new Text(
                        "Lainnya",
                        style: TextStyle(color: Colors.white, fontSize: 12.0),
                      )
                    ],
                  ),
                ],
              ),
            )
          ],
        ));
  }

// box untuk menampung menu dari list menu yang sudah dibuat
  Widget _buildLetsgoServiceMenu() {
    return new SizedBox(
        width: double.infinity,
        height: 220.0,
        child: new Container(
            margin: EdgeInsets.only(top: 15.0, bottom: 8.0),
            child: GridView.builder(
                physics: ClampingScrollPhysics(),
                itemCount: 8,
                gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 4),
                itemBuilder: (context, position) {
                  return _rowLetsService(_letsServiceList[position]);
                })));
  }

// List untuk menu yang akan ditampilkan di letgo menu
  List<LetsService> _letsServiceList = [];

  // list untuk menu letsfood
  List<Food> _letsFoodFeaturedList = [];

  @override
  void initState() {
    super.initState();
// ini siasi unutk list lets service
    _letsServiceList.add(new LetsService(
        image: Icons.directions_bike,
        color: LetsPalette.menuRide,
        title: "Lets-RIDE"));
    _letsServiceList.add(new LetsService(
        image: Icons.local_car_wash,
        color: LetsPalette.menuCar,
        title: "Lets-CAR"));
    _letsServiceList.add(new LetsService(
        image: Icons.directions_car,
        color: LetsPalette.menuBluebird,
        title: "Lets-BLUEBIRD"));
    _letsServiceList.add(new LetsService(
        image: Icons.restaurant,
        color: LetsPalette.menuFood,
        title: "Lets-FOOD"));
    _letsServiceList.add(new LetsService(
        image: Icons.next_week,
        color: LetsPalette.menuSend,
        title: "Lets-SEND"));
    _letsServiceList.add(new LetsService(
        image: Icons.local_offer,
        color: LetsPalette.menuDeals,
        title: "Lets-DEALS"));
    _letsServiceList.add(new LetsService(
        image: Icons.phonelink_ring,
        color: LetsPalette.menuPulsa,
        title: "Lets-PULSA"));
    _letsServiceList.add(new LetsService(
        image: Icons.apps, color: LetsPalette.menuOther, title: "LAINNYA"));
    _letsServiceList.add(new LetsService(
        image: Icons.shopping_basket,
        color: LetsPalette.menuShop,
        title: "Lets-SHOP"));
    _letsServiceList.add(new LetsService(
        image: Icons.shopping_cart,
        color: LetsPalette.menuMart,
        title: "Lets-MART"));
    _letsServiceList.add(new LetsService(
        image: Icons.local_play,
        color: LetsPalette.menuTix,
        title: "Lets-TIX"));

    // inisiasi untuk lets food
    _letsFoodFeaturedList.add(new Food(
        title: "Pisang Goreng",
        image:
            "https://www.goodnewsfromindonesia.id/wp-content/uploads/images/source/faridhahuda/resep-pisang-goreng-pasir.jpg"));
    _letsFoodFeaturedList.add(new Food(
        title: "Pecel Lele",
        image:
            "https://www.goodnewsfromindonesia.id/wp-content/uploads/images/source/faridhahuda/Cara-Membuat-Pecel-Lele-dan-Bumbu-Sambel-nya.jpg"));
    _letsFoodFeaturedList.add(new Food(
        title: "Tempe Goreng",
        image:
            "https://www.goodnewsfromindonesia.id/wp-content/uploads/images/source/faridhahuda/resep-tempe-goreng-sederhana-tanpa-tepung-gurih-nan-lezat.jpg"));
    _letsFoodFeaturedList.add(new Food(
        title: "Sate Ayam",
        image:
            "https://www.goodnewsfromindonesia.id/wp-content/uploads/images/source/faridhahuda/543defb8b5718.jpg"));
    _letsFoodFeaturedList.add(new Food(
        title: "Soto Ayam",
        image:
            "https://www.goodnewsfromindonesia.id/wp-content/uploads/images/source/faridhahuda/Resep-Soto-Ayam-Bumbu-Kuning-Paling-Enak.jpg"));
  }

// membuat sebuah box untuk icon dalam bentuk row
  Widget _rowLetsService(LetsService letsService) {
    return InkWell(
      onTap: () {
        Navigator.push(context,
            MaterialPageRoute(builder: (context) => InputPemesananPage()));
      },
      child: new Container(
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            new Container(
              decoration: new BoxDecoration(
                  border: Border.all(color: LetsPalette.grey200, width: 1.0),
                  borderRadius:
                      new BorderRadius.all(new Radius.circular(20.0))),
              padding: EdgeInsets.all(12.0),
              child: new Icon(
                letsService.image,
                color: letsService.color,
                size: 32.0,
              ),
            ),
            new Padding(
              padding: EdgeInsets.only(top: 6.0),
            ),
            new Text(letsService.title, style: new TextStyle(fontSize: 10.0))
          ],
        ),
      ),
    );
  }

// box untuk list food menu
  Widget _buildLetsFoodFeatured() {
    return new Container(
      padding: EdgeInsets.fromLTRB(16.0, 16.0, 0.0, 16.0),
      child: new Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          new Text(
            "Lets-FOOD",
            style: new TextStyle(
                fontFamily: "Avilar", fontWeight: FontWeight.bold),
          ),
          new Padding(
            padding: EdgeInsets.only(top: 8.0),
          ),
          new Text(
            "Pilihan Terlaris",
            style: new TextStyle(
                fontFamily: "Avilar", fontWeight: FontWeight.bold),
          ),
          new SizedBox(
            height: 172.0,
            child: new ListView.builder(
              itemCount: _letsFoodFeaturedList.length,
              padding: EdgeInsets.only(top: 12.0),
              physics: new ClampingScrollPhysics(),
              scrollDirection: Axis.horizontal,
              itemBuilder: (context, index) {
                return _rowLetsFoodFeatured(_letsFoodFeaturedList[index]);
              },
            ),
          ),
        ],
      ),
    );
  }

  // untuk menampung list dari list food kedalam bentuk row
  Widget _rowLetsFoodFeatured(Food food) {
    return new Container(
      margin: EdgeInsets.only(right: 16.0),
      child: new Column(
        children: <Widget>[
          new ClipRRect(
            borderRadius: new BorderRadius.circular(8.0),
            child: new Image.network(
              food.image,
              scale: 1.0,
              cacheHeight: 132,
              cacheWidth: 132,
            ),
          ),
          new Padding(
            padding: EdgeInsets.only(top: 8.0),
          ),
          new Text(
            food.title,
          ),
        ],
      ),
    );
  }
}
