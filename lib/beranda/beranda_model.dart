import 'package:flutter/material.dart';

class LetsService {
  IconData image;
  Color color;
  String title;

  LetsService({this.image, this.title, this.color});
}

class Food {
  String title;
  String image;
  Food({this.title, this.image});
}
