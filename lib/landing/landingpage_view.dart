import 'package:flutter/material.dart';
import 'package:lets_go/beranda/beranda_view.dart';
import 'package:lets_go/constant.dart';

import 'package:lets_go/page/about.dart';

import 'package:lets_go/page/profile.dart';
import 'package:lets_go/page/riwayatPesan.dart';

class LandingPage extends StatefulWidget {
  @override
  _LandingPageState createState() => _LandingPageState();
}

class _LandingPageState extends State<LandingPage> {
  int _bottomNavCurrentIndex = 0;
  List<Widget> _container = [
    new BerandaPage(),
    new RiwayatPemesananView(),
    new Profile(),
    new AboutPage(),
  ];

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        body: _container[_bottomNavCurrentIndex],
        bottomNavigationBar: _buildBottomNavigation());
  }

  Widget _buildBottomNavigation() {
    return new BottomNavigationBar(
      type: BottomNavigationBarType.fixed,
      onTap: (index) {
        setState(() {
          _bottomNavCurrentIndex = index;
        });
      },
      currentIndex: _bottomNavCurrentIndex,
      items: [
        // home
        BottomNavigationBarItem(
          activeIcon: new Icon(
            Icons.home,
            color: LetsPalette.menuRide,
          ),
          icon: new Icon(
            Icons.home,
            color: Colors.grey,
          ),
          title: new Text(
            'Beranda',
          ),
        ),
        // tambah data
        BottomNavigationBarItem(
          activeIcon: new Icon(
            Icons.assignment,
            color: LetsPalette.menuRide,
          ),
          icon: new Icon(
            Icons.assignment,
            color: Colors.grey,
          ),
          title: new Text('Riwayat Pemesanan'),
        ),
        // information
        BottomNavigationBarItem(
          activeIcon: new Icon(
            Icons.person_pin_rounded,
            color: LetsPalette.menuRide,
          ),
          icon: new Icon(
            Icons.person_pin_rounded,
            color: Colors.grey,
          ),
          title: new Text('Profile'),
        ),
        // tentang
        BottomNavigationBarItem(
          activeIcon: new Icon(
            Icons.person,
            color: LetsPalette.menuRide,
          ),
          icon: new Icon(
            Icons.person,
            color: Colors.grey,
          ),
          title: new Text('About'),
        ),
      ],
    );
  }
}
