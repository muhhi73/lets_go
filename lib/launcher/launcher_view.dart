import 'package:flutter/material.dart';
import 'dart:async';
import 'package:lets_go/landing/landingpage_view.dart';

class LauncherPage extends StatefulWidget {
  @override
  _LauncherPageState createState() => _LauncherPageState();
}

class _LauncherPageState extends State<LauncherPage> {
  String src =
      "https://payload.cargocollective.com/1/6/216449/4883114/letsgo2.png";

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    startLaunching();
  }

  startLaunching() async {
    var durasi = const Duration(seconds: 1);
    return new Timer(durasi, () {
      Navigator.of(context).pushReplacement(new MaterialPageRoute(builder: (_) {
        return new LandingPage();
      }));
    });
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: new Center(
        child: new Image.network(src),
        heightFactor: 100.0,
        widthFactor: 200.0,
      ),
    );
  }
}
